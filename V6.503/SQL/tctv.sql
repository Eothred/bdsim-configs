# ALL LENGTHS MUST BE GIVEN IN MILLIMETRES
# ALL ROTATIONS MUST BE GIVEN IN RADIANS

DROP DATABASE IF EXISTS HCOLL;
CREATE DATABASE HCOLL;
USE HCOLL;

CREATE TABLE A_BOX (
    POSX          DOUBLE(10,3), #
    POSY          DOUBLE(10,3), #
    POSZ          DOUBLE(10,3), #
    LENGTHX        DOUBLE(10,3), #
    LENGTHY        DOUBLE(10,3), #
    LENGTHZ        DOUBLE(10,3), #
    RED           DOUBLE(10,3), #
    GREEN         DOUBLE(10,3), #
    BLUE          DOUBLE(10,3), #
    VISATT        VARCHAR(32),  # I = INVISIBLE, S = SOLID, W = WIREFRAME
    ROTPSI        DOUBLE(10,3), #
    ROTTHETA      DOUBLE(10,3), #
    ROTPHI        DOUBLE(10,3), #
    MATERIAL      VARCHAR(32),  # MATERIAL, CGA LITERAL NAME
    NAME          VARCHAR(32),   # NAME OF SOLID, LOGICAL, AND PHYSICAL VOLUME
    PARENTNAME    VARCHAR(32),   # NAME OF PARENT
    INHERITSTYLE VARCHAR(32) # set to SUBTRACT if you want to subract from parent volume..
);

# when changing jaw opening, only these two jaws need to be 'repositioned'
# The first value (x position) should be the half gap plus 10.0 (half of the tungsten block thickness)
# The other positions are are child objects of the jaw, so no need to update their positions

INSERT INTO A_BOX VALUES (0.0,  11.0,  0.0,   34.0, 20.0, 1000.0,  0.8, 0.2, 0.2, "S",  0.0, 0.0, 0.0, "Tungsten", "upperjaw", "centerofcollimator", "");
INSERT INTO A_BOX VALUES (0.0, -11.0,  0.0,   34.0, 20.0, 1000.0,  0.8, 0.2, 0.2, "S",  0.0, 0.0, 0.0, "Tungsten", "lowerjaw", "centerofcollimator", "");

INSERT INTO A_BOX VALUES (0.0,  13.15, 0.0,  34.0, 6.3, 1000.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "uppercopp", "upperjaw", "");         
INSERT INTO A_BOX VALUES (0.0, -13.15, 0.0,  34.0, 6.3, 1000.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "lowercopp", "lowerjaw", "");  

CREATE TABLE A_TRAP (
    PARENTNAME    VARCHAR(32), #
    POSX          DOUBLE(10,3), #
    POSY          DOUBLE(10,3), #
    POSZ          DOUBLE(10,3), #
    RED           DOUBLE(10,3), #
    GREEN         DOUBLE(10,3), #
    BLUE          DOUBLE(10,3), #
    VISATT        VARCHAR(32),  # I = INVISIBLE, S = SOLID, W = WIREFRAME
    LENGTHXPLUS   DOUBLE(10,3), #
    LENGTHXMINUS  DOUBLE(10,3), #
    LENGTHYPLUS   DOUBLE(10,3), #
    LENGTHYMINUS  DOUBLE(10,3), #
    LENGTHZ       DOUBLE(10,3), #
    ROTPSI        DOUBLE(10,3), #
    ROTTHETA      DOUBLE(10,3), #
    ROTPHI        DOUBLE(10,3), #
    FIELDX        DOUBLE(10,3), #
    MATERIAL      VARCHAR(32),  # MATERIAL, CGA LITERAL NAME
    NAME          VARCHAR(32)   # NAME OF SOLID, LOGICAL, AND PHYSICAL VOLUME
);

INSERT INTO A_TRAP VALUES ("uppercopp", 0.0, 0.0, -550.0,  0.2, 0.1, 0.9, "S", 34.0, 34.0,  9.0, 43.6, 100, 0.0,0.0,0.0, 0.0, "Copper","upperupstreamcorner");
INSERT INTO A_TRAP VALUES ("uppercopp", 0.0, 0.0,  550.0,  0.2, 0.1, 0.9, "S", 34.0, 34.0, 43.6,  9.0, 100, 0.0,0.0,0.0, 0.0, "Copper","upperdownstreamcorner");
INSERT INTO A_TRAP VALUES ("lowercopp", 0.0, 0.0, -550.0,  0.2, 0.1, 0.9, "S", 34.0, 34.0,  9.0, 43.6, 100, 0.0,0.0,0.0, 0.0, "Copper","lowerupstreamcorner");
INSERT INTO A_TRAP VALUES ("lowercopp", 0.0, 0.0,  550.0,  0.2, 0.1, 0.9, "S", 34.0, 34.0, 43.6,  9.0, 100, 0.0,0.0,0.0, 0.0, "Copper","lowerdownstreamcorner");
# removing some of the trapezoid thingies:
# for some reason, using e.g. upperupstreamcorner as parent does not work properly (weird origin)
INSERT INTO A_BOX VALUES (0.0, 18.15,  -550.0,   34.0, 30.0, 120.0,  0.2, 0.1, 0.1, "W",  0.0, 0.0, 0.0, "Copper", "upperupstreamremoved", "uppercopp", "SUBTRACT");
INSERT INTO A_BOX VALUES (0.0, 18.15,   550.0,   34.0, 30.0, 120.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "upperdownstreamremoved", "uppercopp", "SUBTRACT");
INSERT INTO A_BOX VALUES (0.0,-18.15,  -550.0,   34.0, 30.0, 120.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "lowerupstreamremoved", "lowercopp", "SUBTRACT");
INSERT INTO A_BOX VALUES (0.0,-18.15,   550.0,   34.0, 30.0, 120.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "lowerdownstreamremoved", "lowercopp", "SUBTRACT");