# ALL LENGTHS MUST BE GIVEN IN MILLIMETRES
# ALL ROTATIONS MUST BE GIVEN IN RADIANS

DROP DATABASE IF EXISTS VCOLL;
CREATE DATABASE VCOLL;
USE VCOLL;

CREATE TABLE A_BOX (
    POSX          DOUBLE(10,3), #
    POSY          DOUBLE(10,3), #
    POSZ          DOUBLE(10,3), #
    LENGTHX        DOUBLE(10,3), #
    LENGTHY        DOUBLE(10,3), #
    LENGTHZ        DOUBLE(10,3), #
    RED           DOUBLE(10,3), #
    GREEN         DOUBLE(10,3), #
    BLUE          DOUBLE(10,3), #
    VISATT        VARCHAR(32),  # I = INVISIBLE, S = SOLID, W = WIREFRAME
    ROTPSI        DOUBLE(10,3), #
    ROTTHETA      DOUBLE(10,3), #
    ROTPHI        DOUBLE(10,3), #
    MATERIAL      VARCHAR(32),  # MATERIAL, CGA LITERAL NAME
    NAME          VARCHAR(32),   # NAME OF SOLID, LOGICAL, AND PHYSICAL VOLUME
    PARENTNAME    VARCHAR(32)   # NAME OF PARENT
);
                      
INSERT INTO A_BOX VALUES (0.0, 9.3, 500.0,  100.0, 5.0, 900.0,  0.8, 0.2, 0.2, "S",  0.0, 0.0, 0.0, "Tungsten", "upperjaw", "");
INSERT INTO A_BOX VALUES (0.0, 31.8, 500.0,  100.0, 40.0, 1000.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "uppercopp", "");  
                      
INSERT INTO A_BOX VALUES (0.0, -9.3, 500.0,  100.0, 5.0, 900.0,  0.8, 0.2, 0.2, "S",  0.0, 0.0, 0.0, "Tungsten", "lowerjaw", "");
INSERT INTO A_BOX VALUES (0.0, -31.8, 500.0,  100.0, 40.0, 1000.0,  0.2, 0.1, 0.9, "S",  0.0, 0.0, 0.0, "Copper", "lowercopp", "");  

CREATE TABLE A_TRAP (
    PARENTNAME    VARCHAR(32), #
    POSX          DOUBLE(10,3), #
    POSY          DOUBLE(10,3), #
    POSZ          DOUBLE(10,3), #
    RED           DOUBLE(10,3), #
    GREEN         DOUBLE(10,3), #
    BLUE          DOUBLE(10,3), #
    VISATT        VARCHAR(32),  # I = INVISIBLE, S = SOLID, W = WIREFRAME
    LENGTHXPLUS   DOUBLE(10,3), #
    LENGTHXMINUS  DOUBLE(10,3), #
    LENGTHYPLUS   DOUBLE(10,3), #
    LENGTHYMINUS  DOUBLE(10,3), #
    LENGTHZ       DOUBLE(10,3), #
    ROTPSI        DOUBLE(10,3), #
    ROTTHETA      DOUBLE(10,3), #
    ROTPHI        DOUBLE(10,3), #
    FIELDX        DOUBLE(10,3), #
    MATERIAL      VARCHAR(32),  # MATERIAL, CGA LITERAL NAME
    NAME          VARCHAR(32)   # NAME OF SOLID, LOGICAL, AND PHYSICAL VOLUME
);
